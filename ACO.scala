package project

import project._
import scala.util.Random
import scala.collection.mutable.MutableList
import java.util.Calendar

class ACO(var m: Int) {

  var ants: List[Ant] = Nil

  for (i <- 0 to m - 1) { //m= # of ants, here 25 ants
    var i = new Ant
    i.capacity = VRP.capacity
    ants = i :: ants
  }
  ants = ants.reverse

  var dimension = VRP.dimension //32
  var nodes: List[Int] = Nil
  for (i <- 0 to (dimension - 1)) {
    nodes = (i + 1) :: nodes //nodes start at 1, there is no node 0
  }
  nodes = nodes.reverse

  var nodesNorm: List[Int] = Nil
  nodes.foreach { n =>
    var m: Int = n - 1
    nodesNorm = m :: nodesNorm
  }
  nodesNorm = nodesNorm.reverse //ok

  val depots: List[Int] = VRP.altDepot
  //NEW
  var depotsNorm: List[Int] = Nil
  depots.foreach { n =>
    var m: Int = n - 1
    depotsNorm = m :: depotsNorm
  }
  depotsNorm = depotsNorm.reverse

  //set customers = VRP.nodes 
  var distMatrix = VRP.distMatrix
  var iterations = 50 //1 for debugging/testing, TODO: try more, e.g. 5 later

  var nodesExcDepots = nodesNorm.filterNot(n => depotsNorm.contains(n))

  //  var pheroIncr: Double = 0.0001 //0.001 //NEW

  //params for eqns (1) to (4):
  //(1)

  //NEW, feb 27: SKIP ALL THE CLOSEST -neighbour heuristic (not sure whether it should respect capacities), set L2 known optimal:
  var L2 = VRP.solCost

  System.err.println("L2 = " + L2)
  System.err.println("")

  val t0: Double = 1.0 / (L2 * (dimension - 1)) //dimension-1
  System.err.println("t0= " + t0)

  var randomGenQ = new Random()
  var q = randomGenQ.nextDouble
  var q0: Double = 0.9 //means eqn (2) is called rarely, since most numbers ~U[0,1] are <0.9
  var beta: Double = 2.3
  //val t0: Double = 1.0 / VRP.solCost // 1/(best known distance) = 1/784 = 0.0012755102040816326 

  //(2)
  var randomGenS2 = new Random
  var s2 = randomGenS2.nextDouble //the pseudo-random RV ~ U[0,1]

  var sum: Double = 0.0
  def sigmaSum(list: List[Double], l: Int, u: Int): Double = { //big-sigma sum with lower and upper bounds
    if (l == u) {
      sum = list(u)
      return sum
    } else {
      if (l < u) {
        sum = {
          list(l) +
            sigmaSum(list, l + 1, u)
        }
      }
      return sum
    }
  }
  //(3)
  val alpha: Double = 0.1

  var iterSolutionList: List[Double] = Nil

  def chooseNext(k: Ant, pheroMatrix: List[List[Double]], hetaMatrix: List[List[Double]]): Int = {

    var eligibleNodes = nodesExcDepots.filterNot(n => k.visitedNodes.contains(n))

    assert(k.visitedNodes.distinct.length <= dimension)
    //chooseNext customer, BUILD A JOB SEQUENCE, I.E. FIND ALL CUSTOMERS
    q = randomGenQ.nextDouble()
    //            System.err.println("start of while, \nq =" + q)
    if (q <= q0) {
      var us = nodesExcDepots.filter(n => !k.visitedNodes.contains(n)) //TODO: isn't this eligibleNodes from above?
      var criterion1List: List[(Double, Int)] = Nil
      var criterion1bList: List[Double] = Nil

      //eqn (1):
      if (us != Nil) { //if some nodes!=depot still left to visit
        us.foreach { u =>
          var criterion1 = { pheroMatrix(k.location)(u) } * Math.pow({ hetaMatrix(k.location)(u) }, beta)
          criterion1List = (criterion1, u) :: criterion1List
          criterion1bList = criterion1 :: criterion1bList
        }
        criterion1List = criterion1List.reverse
        var filteredCriterion1List = criterion1List.filter(c1 => c1._1 != Double.PositiveInfinity && c1._1 != Double.NegativeInfinity)
        var filteredCriterion1bList = criterion1bList.filter(c => c != Double.PositiveInfinity && c != Double.NegativeInfinity)
        var tupleMax = filteredCriterion1List.find(c1 => (c1._1) == filteredCriterion1bList.max)
        //                System.err.println("tupleMax from eqn(1): " + tupleMax) //None. b/c infinity is the max.
        if (tupleMax != None) k.nextDestination = (tupleMax.get)._2
        //                System.err.println("k.nextDestination from eqn(1): " + k.nextDestination)
      } else { //if visited all nodes there are
        k.nextDestination = depotsNorm.head
      }
    } else {
      //eqn (2):

      var probList: List[Double] = Nil
      var probMatrix: List[List[Double]] = Nil

      for (i <- 0 to (dimension - 1)) {
        probList = Nil
        for (j <- 0 to (dimension - 1)) {
          var us = eligibleNodes
          if (us.contains(j)) { //was: if (!k.visitedNodes.contains(j) && !depotsNorm.contains(j))
            var denomElements: List[Double] = Nil
            us.foreach { u =>
              var denomElement = { pheroMatrix(k.location)(u) } * Math.pow({ hetaMatrix(k.location)(u) }, beta)
              denomElements = denomElement :: denomElements
            }
            denomElements = denomElements.reverse
            val prob = { { pheroMatrix(k.location)(j) } * Math.pow({ hetaMatrix(k.location)(j) }, beta) } / denomElements.sum
            probList = prob :: probList
          } else probList = 0.0 :: probList
        }
        probList = probList.reverse
        probMatrix = probList :: probMatrix
      }
      probMatrix = probMatrix.reverse // all 0s.
      //probMatrix has been constructed, needs to be recalculated for each chooseNext, because pheroMatrix and visited nodes change.
      // also pheroMatrix needs to be recalculated for each chooseNext (--CHECK)

      def invTransform(n: Int): Int = {

        if (n < probMatrix(k.location).length) {
          if (sigmaSum(probMatrix(k.location), 0, (n - 1)) <= s2
            & s2 < sigmaSum(probMatrix(k.location), 0, n)) {
            //                    System.err.println("sigmaSum(probMatrix(k.location), 0, (n - 1): " + sigmaSum(probMatrix(k.location), 0, (n - 1)))
            //                    System.err.println("s2: " + s2)
            //                    System.err.println("sigmaSum(probMatrix(k.location), 0, n): " + sigmaSum(probMatrix(k.location), 0, n))
            return n //? //eligibleNodes(n) 
          } else {
            //                    System.err.println("sigmaSum(probMatrix(k.location), 0, (n - 1): " + sigmaSum(probMatrix(k.location), 0, (n - 1)))
            //                    System.err.println("s2: " + s2)
            //                    System.err.println("sigmaSum(probMatrix(k.location), 0, n): " + sigmaSum(probMatrix(k.location), 0, n))
            return invTransform(n + 1)
          }
        } else {
          return depotsNorm.head
        }
      }
      s2 = randomGenS2.nextDouble
      //              System.err.println("s2: " + s2)
      //              System.err.println("eligible nodes: " + eligibleNodes)
      if (eligibleNodes != Nil && s2 < probMatrix(k.location)(eligibleNodes(0))) {
        k.nextDestination = eligibleNodes(0)
      } else {
        k.nextDestination = invTransform(1)
      }
    } //end of eqn (2)

    return k.nextDestination
  } //end of chooseNext

  def ACOalg() = {

    //var calendar: Calendar = Calendar.getInstance()
    var timeAtStart = System.currentTimeMillis() / 1000 //seconds

    //initialize all trails to t0:
    //    var pheroList: List[Double] = Nil //one row of tau iu, for all u not in Mk //moved inside outer for below
    var pheroMatrix: List[List[Double]] = Nil //tau iu, for all i and all u not in Mk

    for (i <- 0 to (dimension - 1)) {
      var pheroList: List[Double] = Nil
      for (j <- 0 to (dimension - 1)) {
        val phero = t0
        pheroList = phero :: pheroList
      }
      pheroList = pheroList.reverse
      pheroMatrix = pheroList :: pheroMatrix
    }
    pheroMatrix = pheroMatrix.reverse

    //initialize heta i-u = 1/(distance i-u)
    var hetaList: List[Double] = Nil
    var hetaMatrix: List[List[Double]] = Nil

    distMatrix.foreach { l =>
      hetaList = Nil //!
      l.foreach { d =>
        val heta = 1 / d
        hetaList = heta :: hetaList
      }
      hetaList = hetaList.reverse
      hetaMatrix = hetaList :: hetaMatrix
    }
    hetaMatrix = hetaMatrix.reverse

    for (iteration <- 0 to (iterations - 1)) {

      System.out.println("#" + iteration + " iteration")

      ants.foreach { k =>

        System.out.println("")
        System.out.println("Ant: " + k)

        k.nextDestination = depotsNorm.head // node 1 is the depot, nodeNorm 0 is the depot
        k.setLocation(depotsNorm.head)
        k.visitedNodes = Nil
        k.routeMatrix = Nil
        var routeIndex = -1
        var eligibleNodes = nodesExcDepots

        k.solutionDistance = 0 //Pretty print

        while (k.visitedNodes.distinct.length <= dimension & eligibleNodes != Nil) { //until one full solution is completed

          //capacity-related stuff:
          k.location = depotsNorm.head
          k.visitedNodes = k.location :: k.visitedNodes
          eligibleNodes = eligibleNodes.filterNot(n => k.visitedNodes.contains(n)) //NEW
          k.load = 0
          routeIndex += 1

          var routeNodes: List[Int] = Nil

          while (k.load <= k.capacity & k.visitedNodes.distinct.length <= dimension & eligibleNodes != Nil) { //i.e. for each route
            //System.err.println("k.visitedNodes.distinct.length: "+k.visitedNodes.distinct.length)
            k.nextDestination = chooseNext(k, pheroMatrix, hetaMatrix)
            k.load += VRP.nodeDemandArray(k.nextDestination) //No off-by-1.

            if (k.load <= k.capacity) { //if node doesn't overflow the limit, go there

              //deposit pheromone on arc traversed --if location = last customer, look at else below

              //              System.err.println("loc: " + k.location + ", nextDest: " + k.nextDestination)
              //              val incrementedPhero: Double = { pheroMatrix(k.location)(k.nextDestination) } + pheroIncr
              //              pheroMatrix = pheroMatrix.updated(k.location, pheroMatrix(k.location).updated(k.nextDestination, incrementedPhero))

              k.setLocation(k.nextDestination) //move to next customer - set location to nextDestination chosen in previous step
              k.visitedNodes = k.location :: k.visitedNodes
              routeNodes = k.location :: routeNodes
              eligibleNodes = eligibleNodes.filterNot(n => n == k.location) //NEW

              //              System.err.println("Ant "+k)
              //              System.err.println("routeIndex "+routeIndex)              
              //              System.err.println("routeNodes: "+routeNodes)
              //              System.err.println("k.visitedNodes: " + k.visitedNodes)
              //              System.err.println("sorted k.visitedNodes: " + k.visitedNodes.sort((e1, e2) => (e1 < e2)))

              //k.routeMatrix = k.routeMatrix.updated(routeIndex, k.location :: k.routeMatrix(routeIndex))
            }
            //            else {
            //              val incrementedPhero: Double = { pheroMatrix(k.location)(depotsNorm.head) } + pheroIncr
            //              pheroMatrix = pheroMatrix.updated(k.location, pheroMatrix(k.location).updated(depotsNorm.head, incrementedPhero))
            //
            //            }
          } //end of inner while (for each route)

          //          if (eligibleNodes.length == 0) {
          //            val incrementedPhero: Double = { pheroMatrix(k.location)(depotsNorm.head) } + pheroIncr
          //            pheroMatrix = pheroMatrix.updated(k.location, pheroMatrix(k.location).updated(depotsNorm.head, incrementedPhero))
          //
          //          }

          routeNodes = routeNodes.reverse
          k.routeMatrix = routeNodes :: k.routeMatrix

          //Pretty Print stuff:

          //denorm means denormalised:

          System.out.println("")
          System.out.println("")
          System.out.println("route # " + routeIndex + ": ")
          //          System.out.println("routeNodes: " + routeNodes)
          //denorm:
          System.out.print("routeNodes: ")
          routeNodes.foreach { n =>
            System.out.print(n + 1 + " ")
          }
          System.out.println("")

          var cumulativeDist: Double = 0
          var cumulativeDemand: Int = 0
          var dist: Double = 0
          for (i <- 0 to routeNodes.length - 1) { //CHECK this "length-2" everywhere, does it calculate ALL distances, even for last node in list?
            if (i == 0) {
              var distToDepot = distMatrix(depotsNorm.head)(routeNodes(i))
              var pheroToDepot = pheroMatrix(depotsNorm.head)(routeNodes(i))
              //              System.out.print(distToDepot + "d, " + pheroToDepot + "p - " + routeNodes(i) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")")
              //denorm:
              System.out.print(distToDepot + "d, " + pheroToDepot + "p - " + (routeNodes(i) + 1) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")")
              dist += distToDepot
            } else if (i == (routeNodes.length - 1)) {
              dist = distMatrix(routeNodes(i - 1))(routeNodes(i))
              var phero = pheroMatrix(routeNodes(i - 1))(routeNodes(i))
              var distToDepot: Double = distMatrix(routeNodes(i))(depotsNorm.head)
              var pheroToDepot = pheroMatrix(routeNodes(i))(depotsNorm.head)
              //              System.out.print(" - " + dist + "d, " + phero + "p - " + routeNodes(i) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")" + " - " + distToDepot + "d, " + pheroToDepot + "p")
              //denorm:
              System.out.print(" - " + dist + "d, " + phero + "p - " + (routeNodes(i) + 1) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")" + " - " + distToDepot + "d, " + pheroToDepot + "p")

              dist += distToDepot
            } else {
              dist = distMatrix(routeNodes(i - 1))(routeNodes(i))
              var phero = pheroMatrix(routeNodes(i - 1))(routeNodes(i))
              //              System.out.print(" - " + dist + "d, " + phero + "p - " + routeNodes(i) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")")
              //denorm:
              System.out.print(" - " + dist + "d, " + phero + "p - " + (routeNodes(i) + 1) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")")
            }
            //
            //            if (i == routeNodes.length - 1) {
            //              System.out.print(dist + "d") //dist from last node to depot
            //            }

            //            try {
            ////              dist = Math.rint(distMatrix(routeNodes(i))(routeNodes(i - 1)))
            //              dist = distMatrix(k.visitedNodes(i))(k.visitedNodes(i - 1))
            //            } catch {
            //              case e: IndexOutOfBoundsException =>
            ////                dist = Math.rint(distMatrix(routeNodes(i))(depotsNorm.head))
            //                dist = distMatrix(k.visitedNodes(i))(depotsNorm.head)
            //            }
            cumulativeDist += dist
            cumulativeDemand += VRP.nodeDemandArray(routeNodes(i))
          }
          assert(cumulativeDemand <= VRP.capacity)

          k.solutionDistance += cumulativeDist

          System.out.println("")
          System.out.println("cumulative Distance (for each route): " + cumulativeDist)
          System.out.println("cumulative Demand: " + cumulativeDemand)

          //          System.out.println("k.visitedNodes: " + k.visitedNodes)
          //          System.out.println("sorted k.visitedNodes: " + k.visitedNodes.sort((e1, e2) => (e1 < e2)))
          //denorm:
          System.out.println("")
          System.out.print("k.visitedNodes: ")
          k.visitedNodes.foreach { n =>
            System.out.print((n + 1) + " ")
          }
          System.out.println("")
          val sorted = k.visitedNodes.sort((e1, e2) => (e1 < e2))
          System.out.print("sorted k.visitedNodes: ")
          sorted.foreach { n =>
            System.out.print((n + 1) + " ")
          }

          System.out.println("")

          //var eligibleNodes2 = nodesExcDepots.filterNot(n => k.visitedNodes.contains(n))

          //            System.err.println("end of while: nextDestination= " + k.nextDestination)
          //            System.err.println("end of while: k.visitedNodes = " + k.visitedNodes)
          //System.err.println("end of while: k.visitedNodes sorted = " + k.visitedNodes.sort((e1, e2) => (e1 < e2)))

          //            System.err.println("end of while: k.visitedNodes.length = " + k.visitedNodes.length)
          //            System.err.println("end of while: eligibleNodes = " + eligibleNodes)

        } //end of  outer while

        //go back to the depot once eligibleNodes = Nil:
        k.nextDestination = depotsNorm.head
        k.visitedNodes = k.nextDestination :: k.visitedNodes

        k.visitedNodes = k.visitedNodes.reverse
        k.routeMatrix = k.routeMatrix.reverse

        System.out.println("solutionDistance: " + k.solutionDistance)

        //        System.err.println("final k.visitedNodes: " + k.visitedNodes)
        //        System.err.println("final sorted k.visitedNodes: " + k.visitedNodes.sort((e1, e2) => (e1 < e2)))

        // update ant's distance with the new arc's distance cost:
        // TODO LATER(?): decrease pheromone proport. to distance btw 2 nodes

        //local trail update -eqn (3)
        // shouldn't this be run for all arcs in visitedNodes??? --yes

        //update phero on all arcs:
        for (i <- 0 to (k.visitedNodes.length - 2)) { //NEW  took away a -1
          val newPhero: Double = (1 - alpha) * { pheroMatrix(k.visitedNodes(i))(k.visitedNodes(i + 1)) } + alpha * t0 //was =t0 again !
          pheroMatrix = pheroMatrix.updated(k.visitedNodes(i), pheroMatrix(k.visitedNodes(i)).updated(k.visitedNodes(i + 1), newPhero))
        }

      } //end of ant loop

      // Choose best solution out of m ants

      var tourCostList: List[Double] = Nil
      //var tourCost: Double = 0.0
      ants.foreach { a =>
        a.tourCost = 0.0
        for (i <- 0 to (a.visitedNodes.length - 2)) {
          a.tourCost = distMatrix(a.visitedNodes(i))(a.visitedNodes(i + 1)) + a.tourCost
        }
        System.out.println("tourcost for ant " + a + " = " + a.tourCost + ", while solutionDistance = " + a.solutionDistance)
        //        System.err.println("a.visitedNodes = " + a.visitedNodes)
        tourCostList = a.tourCost :: tourCostList //for all ants
      }
      tourCostList = tourCostList.reverse

      System.out.println("")
      System.out.println("tourCostList: " + tourCostList)

      var bestSolutionAnt: Ant = null
      var bestSolutionCost = tourCostList.min
      var L = bestSolutionCost
      ants.foreach { a =>
        if (a.tourCost == bestSolutionCost)
          bestSolutionAnt = a
      }
      assert(bestSolutionAnt != null)

      var bestSolutionPath = bestSolutionAnt.visitedNodes

      //      System.out.println("bestSolutionPath: " + bestSolutionPath)
      //      System.out.println("bestSolutionAnt.routeMatrix: " + bestSolutionAnt.routeMatrix)
      //denorm:
      System.out.println("")
      System.out.print("bestSolutionPath: ")
      bestSolutionPath.foreach { n =>
        System.out.print((n + 1) + " ")
      }
      System.out.println("")
      System.out.print("bestSolutionAnt.routeMatrix: ")
      bestSolutionAnt.routeMatrix.foreach { l =>
        l.foreach { n =>
          System.out.print((n + 1) + " ")
        }
        System.out.print(", ")
      }
      System.out.println("")

      // global trail update for this solution - eqn (4) //NEW
      // increase pheromone on all arcs in best route.

      for (i <- 0 to (bestSolutionPath.length - 2)) {
        pheroMatrix = pheroMatrix.updated(bestSolutionPath(i), pheroMatrix(bestSolutionPath(i)).updated(bestSolutionPath(i + 1), (1 - alpha) * { pheroMatrix(bestSolutionPath(i))(bestSolutionPath(i + 1)) } + alpha * (1 / L)))
      }

      iterSolutionList = bestSolutionCost :: iterSolutionList
      System.out.println("bestSolutionCost: " + bestSolutionCost)
      System.out.println("iterSolutionList: " + iterSolutionList)
      System.out.println("")

      //NEW: print the new pheromone values for the best solution, after being updated by eqn. 4:
      System.out.println("Pheromone values for the best solution, after being updated by eqn. 4:")
      bestSolutionAnt.routeMatrix.foreach { routeNodes =>
        for (i <- 0 to routeNodes.length - 1) { //CHECK this "length-2" everywhere, does it calculate ALL distances, even for last node in list?
          if (i == 0) {
            var pheroToDepot = pheroMatrix(depotsNorm.head)(routeNodes(i))
            //              System.out.print(distToDepot + "d, " + pheroToDepot + "p - " + routeNodes(i) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")")
            //denorm:
            System.out.print(pheroToDepot + "p - " + (routeNodes(i) + 1) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")")

          } else if (i == (routeNodes.length - 1)) {
            var phero = pheroMatrix(routeNodes(i - 1))(routeNodes(i))
            var pheroToDepot = pheroMatrix(routeNodes(i))(depotsNorm.head)
            //              System.out.print(" - " + dist + "d, " + phero + "p - " + routeNodes(i) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")" + " - " + distToDepot + "d, " + pheroToDepot + "p")
            //denorm:
            System.out.print(" - " + phero + "p - " + (routeNodes(i) + 1) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")" + " - " + pheroToDepot + "p")

          } else {
            var phero = pheroMatrix(routeNodes(i - 1))(routeNodes(i))
            //              System.out.print(" - " + dist + "d, " + phero + "p - " + routeNodes(i) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")")
            //denorm:
            System.out.print(" - " + "d, " + phero + "p - " + (routeNodes(i) + 1) + "(" + VRP.nodeDemandArray(routeNodes(i)) + ")")
          }
        }
      }

      System.out.println("")

      //NEW CODE: -- is this correct/ok? improvements between iterations now smaller (?)
      if (iteration == 4 | iteration == 9 | iteration == 29 | iteration == 49 | iteration == 99 | iteration == 199 | iteration == 299 | iteration == 399 | iteration == 499) {
        var iterBestSol = iterSolutionList.min
        System.out.println("At iteration #" + iteration + ": ")
        System.out.println("best solution: " + iterBestSol)
        System.out.println("Known optimal solution: " + VRP.solCost)
        var percentOff = (iterBestSol - VRP.solCost) / VRP.solCost
        System.out.println("Off from optimal by: " + percentOff * 100 + " %.")
        var timeAtEnd = System.currentTimeMillis() / 1000 //seconds
        var duration = timeAtEnd - timeAtStart
        System.out.println("Time at Start: " + timeAtStart + " s, " + " Time at End: " + timeAtEnd + " s.")
        System.out.println("Time taken this far: " + duration + " s.")
      }

    } //end of iteration

    //choose best solution out of all iterations
    var iterBestSol = iterSolutionList.min
    System.out.println("best solution: " + iterBestSol)
    System.out.println("Known optimal solution: " + VRP.solCost)
    var percentOff = (iterBestSol - VRP.solCost) / VRP.solCost
    System.out.println("Off from optimal by: " + percentOff * 100 + " %.")
    var timeAtEnd = System.currentTimeMillis() / 1000 //seconds
    var duration = timeAtEnd - timeAtStart
    System.out.println("Time at Start: " + timeAtStart + " s, " + " Time at End: " + timeAtEnd + " s.")
    System.out.println("Time taken by ACOalg: " + duration + " s.")
  }
}