package project

import java.io.Reader
import java.io.BufferedReader
import java.io.FileReader
import java.io.File
import java.util.Scanner
import project.ACO

object VRP {

  //params

  //to read in data from file
  //"/Users/mimie/mimh NEW/St John's/year3/Part II Project/Actual/Datasets/Augerat et al instances - Sets A, B and P/Set A/A-VRP/A-n45-k6.vrp"
  var datasetsPath = "/Users/mimie/mimh NEW/St John's/year3/Part II Project/Actual/Datasets"
  var augeral_A: String = datasetsPath + "/Augerat et al instances - Sets A, B and P/Set A/A-VRP"
  var augeral_B: String = datasetsPath + "/Augerat et al instances - Sets A, B and P/Set B/B-VRP"
  var augeral_P: String = datasetsPath + "/Augerat et al instances - Sets A, B and P/Set P/P-VRP"
  var ce: String = "/Users/mimie/mimh NEW/St John's/year3/Part II Project/Actual/Datasets/Christofides and Eilon/CE-VRP"

  var filename: File = new File(augeral_A + "/A-n32-k5.vrp")
  var reader: Reader = new FileReader(filename)
  var buffRdr = new BufferedReader(reader)

  var name, comment, problemType, edgeWeightType = ""
  var dimension, capacity = 0

  type t = (Int, Int)
  var nodeCoords = new Array[t](dimension)
  var distList: List[Double] = Nil
  var distMatrix: List[List[Double]] = Nil
  var altDepot: List[Int] = Nil
  var nodeDemandArray = new Array[Int](dimension)

  //to read in best known route distances
  //"/Users/mimie/mimh NEW/St John's/year3/Part II Project/Actual/Datasets/Augerat et al instances - Sets A, B and P/Set A/A-VRP-sol/opt-A-n45-k6"
  var augeral_A_opt: String = datasetsPath + "/Augerat et al instances - Sets A, B and P/Set A/A-VRP-sol"
  var augeral_B_opt: String = datasetsPath + "/Augerat et al instances - Sets A, B and P/Set B/B-VRP-sol"

  var filenameSol: File = new File(augeral_A_opt + "/opt-A-n32-k5")
  var readerSol: Reader = new FileReader(filenameSol)
  var buffRdrSol = new BufferedReader(readerSol)

  var solCost = 0.0
  def readFile(filename: File) = {

    var newline = buffRdr.readLine()
    while (newline != null && newline != "EOF") {

      //NOTE: IDEALLY DON'T RECHECK SOMETHNG ONCE IT'S BEEN SET -- FLAGS!
      //ALSO, GENERALISE: filepath, other possible name-value pairs

      if (newline.startsWith("NAME : ")) {
        name = newline.substring("NAME : ".length)
        System.out.println(name)
      }
      if (newline.startsWith("COMMENT : ")) {
        comment = newline.substring("COMMENT : ".length)
        System.out.println(comment)
      }
      if (newline.startsWith("TYPE : ")) {
        problemType = newline.substring("TYPE : ".length)
        System.out.println(problemType)
      }
      if (newline.startsWith("DIMENSION : ")) {
        dimension = newline.substring("DIMENSION : ".length).toInt
        System.out.println(dimension)
      }
      if (newline.startsWith("EDGE_WEIGHT_TYPE : ")) {
        edgeWeightType = newline.substring("EDGE_WEIGHT_TYPE : ".length)
        System.out.println(edgeWeightType)
      }
      if (newline.startsWith("CAPACITY : ")) {
        capacity = newline.substring("CAPACITY : ".length).toInt
        System.out.println(capacity)
      }

      if (newline.startsWith("NODE_COORD_SECTION")) {

        nodeCoords = new Array[t](dimension) //KEEP HERE b/c of dimension.

        System.out.println("for -coords")
        for (i <- 0 to (dimension - 1) by +1) {
          newline = buffRdr.readLine();
          var linescanner = new Scanner(newline)
          val index = linescanner.nextInt()
          assert(i == index - 1) //
          nodeCoords(i) = (linescanner.nextInt(), linescanner.nextInt())
          System.out.println(index + "'s coords : " + nodeCoords(i))
        }
      }

      if (newline.startsWith("DEMAND_SECTION")) {

        var nodeDemand = new Array[Int](dimension)

        System.out.println("for -demand")
        for (i <- 0 to (dimension - 1) by +1) {
          newline = buffRdr.readLine();
          var linescanner = new Scanner(newline)
          val index = linescanner.nextInt()
          assert(i == index - 1) //
          nodeDemand(i) = (linescanner.nextInt())
          System.out.println(index + "'s demand : " + nodeDemand(i))
          nodeDemandArray = nodeDemand
        }
      }
      if (newline.startsWith("DEPOT_SECTION")) {
        newline = buffRdr.readLine();
        while (!newline.startsWith(" -1")) {
          var linescanner = new Scanner(newline)
          altDepot = linescanner.nextInt() :: altDepot
          System.out.println("altDepot: " + altDepot)
          newline = buffRdr.readLine();
        }
      }
      newline = buffRdr.readLine()
    }
  }

  def calcdistances() = { //use distance formula, save results in a Matrix

    if (edgeWeightType == "EUC_2D" | edgeWeightType == "EUC_2D ") {

      assert(nodeCoords != null)

      nodeCoords.foreach { e1 =>
        nodeCoords.foreach { e2 =>
          val dist = { math.sqrt((e1._1 - e2._1) * (e1._1 - e2._1) + (e1._2 - e2._2) * (e1._2 - e2._2)) }
          distList = dist :: distList //THIS IS 1D, MADE 2D in i loop
          //System.out.println(distList) 
        }
        distList = distList.reverse //NEW!!!
        assert(distList.length == dimension)
        distMatrix = distList :: distMatrix
        distList = Nil
        assert(distList.length == 0)
        System.out.println("at node " + e1 + ", distList= " + distMatrix)
      }
      distMatrix = distMatrix.reverse //NEW!!!
      assert(distMatrix.length == dimension)
      System.out.println("Matrix= " + distMatrix)
    }
  }

  def prettyPrint() = {

    var readerSol2: Reader = new FileReader(filenameSol)
    var buffRdrSol2 = new BufferedReader(readerSol2)
    var newline2 = buffRdrSol2.readLine()
    var routeMatrix: List[List[Int]] = Nil
    var nodesList: List[Int] = Nil

    while (newline2 != null && newline2 != "EOF") {
      if (newline2.startsWith("Route #")) {
        val nodes: String = newline2.substring("Route #_: ".length)
        var nodesArray = nodes.split(" ")
      }

      //      nodesList = nodesList.reverse
      //      routeMatrix = nodesList :: routeMatrix
    }
    //    routeMatrix = routeMatrix.reverse
    //    System.out.println("routeMatrix :" + routeMatrix)
  }

  def readSol(filenameSol: File) = {
    var newline = buffRdrSol.readLine()
    while (newline != null && newline != "EOF") {
      System.out.println(newline)
      if (newline.startsWith("cost ") | newline.startsWith("Cost ")) {
        solCost = newline.substring("cost ".length).toDouble
      }
      newline = buffRdrSol.readLine()
    }
  }

  def main(args: Array[String]) = {
    readFile(filename)
    calcdistances() //calculate distances
    readSol(filenameSol)
    //    prettyPrint()
    val aco = new ACO(25)
    aco.ACOalg()
  }
}