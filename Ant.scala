package project

class Ant {

  var visitedNodes: List[Int] = Nil 
  var distance: Int = 0 //max distance L
  var capacity: Int = 0
  var location: Int = 0
  var nextDestination: Int = 0
  var tourCost:Double =0.0
  var load:Int = 0
  var demand:Int = 0
  var solutionDistance:Double =0

  var routeMatrix: List[List[Int]] = Nil
  
  def setDistance(x: Int) = { distance = x }
  def setCapacity(x: Int) = { capacity = x }
  def setLocation(x: Int) = { location = x }

}